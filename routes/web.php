<?php

use App\Http\Controllers\DiskusiController;
use App\Http\Controllers\JawabanController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('diskusi', DiskusiController::class);
Route::resource('jawaban', JawabanController::class);
Route::get('/', [JawabanController::class,'home']);




