<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    use HasFactory;
    protected $table = 'jawaban';
    protected $fillable = [
        'jawaban','pertanyaan_id'
    ];
    public function diskusi()
     {
        return $this->belongsTo(Diskusi::class);
    }
    public function user()
     {
        return $this->belongsTo(User::class);
    }

}
