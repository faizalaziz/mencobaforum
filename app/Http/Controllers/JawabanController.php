<?php

namespace App\Http\Controllers;
use App\Models\Diskusi;
use App\Models\Jawaban;
use Illuminate\Http\Request;

class JawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jawaban = new Jawaban;
        $jawaban->jawaban = $request->jawaban;
        $jawaban->diskusi_id = $request->diskusi_id;
        $jawaban->user_id = $request->user_id;
        $jawaban->save();
        return redirect('/diskusi/'. $request->diskusi_id);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jawaban_edit = Jawaban::find($id);
        $diskusi_id = Diskusi::find($jawaban_edit->diskusi_id);
        return view('diskusi.jawaban_edit',['diskusi_id'=>$diskusi_id,'jawaban_edit'=>$jawaban_edit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jawaban = Jawaban::find($id);
        $jawaban->jawaban = $request->jawaban;
        $jawaban->update();
        return redirect('/diskusi/'. $jawaban->diskusi_id);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function home(){
        return view('welcome');
    }
}
