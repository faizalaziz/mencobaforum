<?php

namespace App\Http\Controllers;
use App\Models\Diskusi;
use App\Models\Jawaban;
use App\Models\Kategori;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class DiskusiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show','index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diskusi = Diskusi::get();
        return view('diskusi.diskusi',['diskusi'=>$diskusi]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::get();
        return view('diskusi.diskusi_create',['kategori'=>$kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $diskusi = new Diskusi;
        $diskusi->judul = $request->judul;
        $diskusi->pertanyaan = $request->pertanyaan;
        $diskusi->kategori_id = $request->kategori_id;
        $diskusi->user_id = $request->user_id;
        $diskusi->save();
        return redirect('/diskusi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $diskusi_id = Diskusi::find($id);
        return view('diskusi.diskusi_id',['diskusi_id'=>$diskusi_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $diskusi_edit = Diskusi::find($id);
        $kategori = Kategori::get();
        return view('diskusi.diskusi_edit',['diskusi_edit'=>$diskusi_edit,'kategori'=>$kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $diskusi_edit = Diskusi::find($id);
        $diskusi_edit->judul = $request->judul;
        $diskusi_edit->pertanyaan = $request->pertanyaan;
        $diskusi_edit->kategori_id = $request->kategori_id;
        $diskusi_edit->update();
        return redirect('/diskusi/' . $diskusi_edit->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}





