@extends('layouts.app')

@section('content')

<div class="container">
    <div class="card">
        <div class="d-flex justify-content-between">
            
            @auth
                <a href="/diskusi"> Buat atau baca forum</a>
            @endauth
            @guest
            <a href="/home">Login</a>
            <a href="/diskusi">Langsung baca Forum tanpa login</a>

            @endguest
        </div>
    </div>

@endsection