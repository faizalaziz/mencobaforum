@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Pertanyaan</h1>
        <div class="form">
            <form  method="post" action="/diskusi/{{$diskusi_edit->id}}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <input type="text" name="judul" id="judul" value="{{$diskusi_edit->judul}}" class="form-control">
            </div>
            <div class="form-group">
                <textarea name="pertanyaan" id="pertanyaan" cols="30" rows="10" class="form-control">{{$diskusi_edit->pertanyaan}}</textarea>
            </div>

            <div class="form-group">
                <select name="kategori_id" id="kategori_id">
                    @foreach ($kategori as $satukategori)
                        <option value="{{$satukategori->id}}">{{$satukategori->namakategori}}</option>
                    @endforeach    
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success" >simpan perubahan</button>
            </div>

        </form>
        </div>
    </div>
@endsection