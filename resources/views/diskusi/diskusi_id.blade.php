@extends('layouts.app')

@section('content')
<div class="card container">
        <div class="card-header d-flex justify-content-between bg-white">
                <h1><b>{{$diskusi_id->judul}}</b></h1>
        </div>
        <div class="card-header d-flex justify-content-between bg-white">
            <h2><b>{{$diskusi_id->pertanyaan}}</b></h2>
            @auth
                @if ( $diskusi_id->user->id === Auth::id() )
                    <a href="/diskusi/{{$diskusi_id->id}}/edit">Edit Pertanyaan</a>
                @endif 
            @endauth
                
         </div>
        @foreach ($diskusi_id->jawaban as $jawaban)                  
            <div class="card-footer">
                <p>{{$jawaban->jawaban}}</p>
                <a href="#">{{$jawaban->user->name}}</a>
                @auth
                    @if ( $jawaban->user->id === Auth::id() )
                        <a href="/jawaban/{{$jawaban->id}}/edit#jawaban">Edit jawaban</a>
                    @endif 
                @endauth
            </div>
        @endforeach
        <div class="form mt-5">
            <form action="/jawaban" method="post">
                @csrf
                
                @auth
                <textarea name="jawaban" id="jawaban" cols="30" rows="10" class="form-control">Tulis Jawabanmu</textarea>
                <select name="diskusi_id" id="diskusi_id" class="d-none">
                    <option value="{{$diskusi_id->id}}"></option>
                </select>
                <select name="user_id" id="user_id" class="d-none">
                    <option value="{{Auth::id()}}"></option>
                </select>
                <button class="form-control">submit jawaban</button>
                    
                @endauth
                @guest
                    <a href="/login">Login untuk menjawab</a>
                @endguest
            </form>
        </div>
        
    <a class="btn btn-warning" href="/diskusi">kembali ke kumpulan diskusi</a>

</div>
@endsection