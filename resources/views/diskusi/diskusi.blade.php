@extends('layouts.app')

@section('content')

<div class="container">
    <div class="card">
        @foreach ($diskusi as $satudiskusi)
            <div class="card-header d-flex justify-content-between bg-white">
                    <b>{{$satudiskusi->judul}}</b>
                    <a href="#">{{$satudiskusi->kategori->namakategori}}</a>
                    <a href="#">{{$satudiskusi->user->name}}</a>
                    <a href="/diskusi/{{$satudiskusi->id}}">Detail</a>
            </div>
        @endforeach
      </div>
</div>

@endsection


