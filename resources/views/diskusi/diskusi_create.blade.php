@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Buat Pertanyaan</h1>
        <div class="form">
            <form  method="post" action="/diskusi">
            @csrf
            <div class="form-group">
                <input type="text" name="judul" id="judul" placeholder="judul" class="form-control">
            </div>
            <div class="form-group">
                <textarea name="pertanyaan" id="pertanyaan" cols="30" rows="10" class="form-control">Pertanyaanmu?</textarea>
            </div>

            <div class="form-group">
                <select name="kategori_id" id="kategori_id">
                    @foreach ($kategori as $satukategori)
                        <option value="{{$satukategori->id}}">{{$satukategori->namakategori}}</option>
                    @endforeach    
                </select>
            </div>
            <select name="user_id" id="user_id" class="d-none">
                <option value="{{Auth::id()}}"></option>
            </select>
            <div class="form-group">
                <button type="submit" class="btn btn-success" >Submit Pertanyaan</button>
            </div>

        </form>
        </div>
    </div>
@endsection