@extends('layouts.app')

@section('content')
<div class="card container">
        <div class="card-header d-flex justify-content-between bg-white">
                <h1><b>{{$diskusi_id->judul}}</b></h1>
        </div>
        <div class="card-header d-flex justify-content-between bg-white">
            <h2><b>{{$diskusi_id->pertanyaan}}</b></h2>
         </div>
        @foreach ($diskusi_id->jawaban as $jawaban)                  
            <div class="card-footer">
                <p>{{$jawaban->jawaban}}</p>
                <a href="#">{{$jawaban->user->name}}</a>
                @auth
                    @if ( $jawaban->user->id === Auth::id() )
                        <a href="/jawaban/{{$jawaban->id}}/edit">Edit jawaban</a>
                    @endif 
                @endauth
            </div>
        @endforeach
        <div class="form mt-5">
            <form action="/jawaban/{{$jawaban_edit->id}}" method="post">
                @csrf
                @method('put')
                @auth
                <textarea name="jawaban" id="jawaban" cols="30" rows="10" class="form-control">{{$jawaban_edit->jawaban}}</textarea>
                <button class="form-control btn btn-danger">submit perubahan jawaban</button>
                <select name="diskusi_id" id="diskusi_id" class="d-none">
                    <option value="{{$jawaban_edit->id}}"></option>
                </select>
                @endauth
                @guest
                    <a href="/login">Login untuk menjawab</a>
                @endguest
            </form>
        </div>
        
    <a class="btn btn-warning" href="/diskusi">kembali ke kumpulan diskusi</a>

</div>
@endsection




